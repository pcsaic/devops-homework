# Repository for homework.

**Commands list:** <br>

<pre>
docker network create --driver bridge shchasny
docker pull nginx
docker run --name super-nginx -v /conf/nginx-c.conf:/etc/nginx/conf.d -d -p 8080:80 nginx
docker network connect shchasny super-nginx
docker network disconnect bridge super-nginx
docker exec super-nginx bash -c "mkdir alex; touch alex/shchasny.txt; cd alex; echo 'alex shchasny' >> shchasny.txt; cat shchasny.txt"
docker commit super-nginx saicrasoft/super-nginx:latest
docker push saicrasoft/super-nginx:latest
</pre>

**Link to Docker hub:**
https://hub.docker.com/repository/docker/saicrasoft/super-nginx